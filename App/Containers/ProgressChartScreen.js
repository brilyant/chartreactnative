import React, { Component } from 'react'
import { Container, Header, Title, Content, Button, Left, Right, Body, Icon, Text, Subtitle } from 'native-base'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

import {
  ProgressChart
} from 'react-native-chart-kit';

// Styles
import styles from './Styles/ProgressChartScreenStyle'
import { Dimensions } from 'react-native';
import { Metrics } from '../Themes';

class ProgressChartScreen extends Component {
  render () {
    return (
      <Container>
        <Header noLeft>
          <Left />
          <Body>
            <Subtitle>Progress Chart</Subtitle>
          </Body>
          <Right />
        </Header>
        <Content contentContainerStyle={{ flex: 1 }}>
          <ProgressChart
            data={{
              labels: ['Swim', 'Bike', 'Run'], // optional
              data: [0.4, 0.6, 0.8]
            }}
            width={Metrics.screenWidth - 30} // Metrics = dari ignite
            height={(Metrics.screenHeight / 2) - 50} // setengah tinggi layar - 50px
            strokeWidth={16}
            radius={32}
            chartConfig={{
              backgroundColor: '#3F51B5',
              backgroundGradientFrom: '#3F51B5',
              // backgroundGradientTo: '#3F51B5',
              decimalPlaces: 2, // optional, defaults to 2dp
              color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
              labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
              style: {
                borderRadius: 16
              }
            }}
            hideLegend={false}
            style={{
              marginVertical: 8,
              borderRadius: 16,
              alignSelf: 'center'
            }}
          />
        </Content> 
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProgressChartScreen)
