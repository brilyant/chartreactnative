import React, { Component } from 'react'
import { Container, Header, Title, Content, Button, Left, Right, Body, Icon, Text, Subtitle } from 'native-base'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

import {
  PieChart
} from 'react-native-chart-kit';

// Styles
import styles from './Styles/PieChartScreenStyle'
import { Dimensions } from 'react-native';
import { Metrics } from '../Themes';

class PieChartScreen extends Component {
  render () {
    return (
      <Container>
        <Header noLeft>
          <Left />
          <Body>
            <Subtitle>Pie Chart</Subtitle>
          </Body>
          <Right />
        </Header>
        <Content contentContainerStyle={{ flex: 1 }}>
          <PieChart
            data={[
              {
                name: 'Seoul',
                population: 21500000,
                color: 'rgba(131, 167, 234, 1)',
                legendFontColor: 'white',
                legendFontSize: 15
              },
              {
                name: 'Toronto',
                population: 2800000,
                color: 'lime',
                legendFontColor: 'white',
                legendFontSize: 15
              },
              {
                name: 'Beijing',
                population: 527612,
                color: 'red',
                legendFontColor: 'white',
                legendFontSize: 15
              },
              {
                name: 'New York',
                population: 8538000,
                color: '#ffffff',
                legendFontColor: 'white',
                legendFontSize: 15
              },
              {
                name: 'Moscow',
                population: 11920000,
                color: 'rgb(0, 0, 255)',
                legendFontColor: 'white',
                legendFontSize: 15
              }
            ]}
            width={Metrics.screenWidth - 10} // Metrics = dari ignite
            height={(Metrics.screenHeight / 2) - 70} // setengah tinggi layar - 50px
            accessor="population"
            backgroundColor="#3F51B5"
            chartConfig={{
              backgroundColor: '#3F51B5',
              backgroundGradientFrom: '#3F51B5',
              backgroundGradientTo: '#3F51B5',
              decimalPlaces: 2, // optional, defaults to 2dp
              color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
              labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
              style: {
                // borderRadius: 16
              },
               
            }}
             
            style={{
              marginVertical: 8,
              borderRadius: 16,
              alignSelf: 'center'
            }}
          />
        </Content> 
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PieChartScreen)
