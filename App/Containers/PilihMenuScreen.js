import React, { Component } from 'react'
// import { ScrollView, Text, KeyboardAvoidingView } from 'react-native'
import { Container, Header, Title, Content, Button, Left, Right, Body, Icon, Text } from 'native-base'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/PilihMenuScreenStyle'

class PilihMenuScreen extends Component {
  render () {
    //Dokumentasi di :
    // https://www.npmjs.com/package/react-native-chart-kit
    return (
      <Container>
        <Header noLeft>
          <Left>
            <Button transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Test UI Jotform</Title>
          </Body>
          <Right />
        </Header>
        <Content contentContainerStyle={{ flex: 1, justifyContent: 'center' }}> 
          <Button 
            style={{marginHorizontal: 35, marginVertical: 10 }} 
            block
            onPress={() => this.props.navigation.navigate('LineChartScreen')}
          >
            <Text>Line Chart</Text>
          </Button>
          <Button 
            style={{marginHorizontal: 35, marginVertical: 10 }} 
            block
            onPress={() => this.props.navigation.navigate('BarChartScreen')}
          >
            <Text>Bar Chart</Text>
          </Button>

          <Button 
            style={{marginHorizontal: 35, marginVertical: 10 }} 
            block
            onPress={() => this.props.navigation.navigate('PieChartScreen')}
          >
            <Text>Pie Chart</Text>
          </Button>
          <Button 
            style={{marginHorizontal: 35, marginVertical: 10 }} 
            block
            onPress={() => this.props.navigation.navigate('ProgressChartScreen')}
          >
            <Text>Progress Chart</Text>
          </Button>
        </Content> 
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PilihMenuScreen)
