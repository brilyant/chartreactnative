import React, { Component } from 'react'
import { Container, Header, Title, Content, Button, Left, Right, Body, Icon, Text, Subtitle } from 'native-base'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

import {
  LineChart
} from 'react-native-chart-kit';

// Styles
import styles from './Styles/LineChartScreenStyle'
import { Dimensions } from 'react-native';
import { Metrics } from '../Themes';

class LineChartScreen extends Component {
  render () {
    return (
      <Container>
        <Header noLeft>
          <Left />
          <Body>
            <Subtitle>Line Chart</Subtitle>
          </Body>
          <Right />
        </Header>
        <Content contentContainerStyle={{ flex: 1 }}>
          <LineChart
            data={{
              labels: ['January', 'February', 'March', 'April', 'May', 'June'],
              datasets: [
                {
                  data: [
                    Math.random() * 100,
                    Math.random() * 100,
                    Math.random() * 100,
                    Math.random() * 100,
                    Math.random() * 100,
                    Math.random() * 100
                  ]
                }
              ]
            }}
            width={Metrics.screenWidth - 30} // Metrics = dari ignite
            height={(Metrics.screenHeight / 2) - 50} // setengah tinggi layar - 50px
            yAxisLabel="$"
            yAxisSuffix="k"
            yAxisInterval={1} // optional, defaults to 1
            chartConfig={{
              backgroundColor: '#3F51B5',
              backgroundGradientFrom: '#3F51B5',
              backgroundGradientTo: '#3F51B5',
              decimalPlaces: 2, // optional, defaults to 2dp
              color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
              labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
              style: {
                borderRadius: 16
              },
              propsForDots: {
                r: '5',
                strokeWidth: '2',
                stroke: '#ffa726',
                // fill: '#3F51B5'
              }
            }}
            bezier //props bezier menentukan line chart yg melengkung / garis
            style={{
              marginVertical: 8,
              borderRadius: 16,
              alignSelf: 'center'
            }}
          />
        </Content> 
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LineChartScreen)
