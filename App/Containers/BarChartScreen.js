import React, { Component } from 'react'
import { Container, Header, Title, Content, Button, Left, Right, Body, Icon, Text, Subtitle } from 'native-base'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

import {
  BarChart
} from 'react-native-chart-kit';

// Styles
import styles from './Styles/BarChartScreenStyle'
import { Dimensions } from 'react-native';
import { Metrics } from '../Themes';

class BarChartScreen extends Component {
  render () {
    return (
      <Container>
        <Header noLeft>
          <Left />
          <Body>
            <Subtitle>Bar Chart</Subtitle>
          </Body>
          <Right />
        </Header>
        <Content contentContainerStyle={{ flex: 1 }}>
          <BarChart
            style={{
              marginVertical: 8,
              borderRadius: 16,
              alignSelf: 'center'
            }}
            data={{
              labels: ['January', 'February', 'March', 'April', 'May'],
              datasets: [
                {
                  data: [20, 45, 28, 80, 99]
                }
              ]
            }}
            width={Metrics.screenWidth - 30} // Metrics = dari ignite
            height={(Metrics.screenHeight / 2) - 50} // setengah tinggi layar - 50px
            yAxisLabel="$"
            chartConfig={{
              backgroundColor: '#3F51B5',
              backgroundGradientFrom: '#3F51B5',
              backgroundGradientTo: '#3F51B5',
              decimalPlaces: 2, // optional, defaults to 2dp
              color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
              labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
              style: {
                borderRadius: 16, 
              },
              
            }}
            verticalLabelRotation={0}
          />
        </Content> 
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BarChartScreen)
