import { createAppContainer } from 'react-navigation'
import ProgressChartScreen from '../Containers/ProgressChartScreen'
import PieChartScreen from '../Containers/PieChartScreen'
import BarChartScreen from '../Containers/BarChartScreen'
import LineChartScreen from '../Containers/LineChartScreen'
import PilihMenuScreen from '../Containers/PilihMenuScreen' 
import { createStackNavigator } from 'react-navigation-stack'; 

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = createStackNavigator({ 
  ProgressChartScreen: { screen: ProgressChartScreen },
  PieChartScreen: { screen: PieChartScreen },
  BarChartScreen: { screen: BarChartScreen },
  LineChartScreen: { screen: LineChartScreen },
  PilihMenuScreen: { screen: PilihMenuScreen }, 
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'PilihMenuScreen', // <--- 
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default createAppContainer(PrimaryNav)
