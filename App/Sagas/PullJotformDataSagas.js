/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the Infinite Red Slack channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import PullJotformDataActions from '../Redux/PullJotformDataRedux'
import { Toast } from 'native-base'
// import { PullJotformDataSelectors } from '../Redux/PullJotformDataRedux'

export function * pullJotformDataRequest (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(PullJotformDataSelectors.getData)
  // make the call to the api
  const response = yield call(api.pullJotformDataRequest, data)

  // success?
  if (response.ok) {
    Toast.show({
      text: response.data,
      type: 'success'
    })
    yield put(PullJotformDataActions.pullJotformDataSuccess(response.data))
  } else {
    Toast.show({
      text: 'gagal terhubung ke server',
      type: 'danger'
    })
    yield put(PullJotformDataActions.pullJotformDataFailure())
  }
}
