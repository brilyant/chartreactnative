import { takeLatest, all } from 'redux-saga/effects'
import Api_Kita from '../Services/Api_Kita'
import Api_Jotform from '../Services/Api_Jotform'
import FixtureAPI from '../Services/FixtureApi'
import DebugConfig from '../Config/DebugConfig'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { GithubTypes } from '../Redux/GithubRedux'
import { JotFormSubmissionTypes } from '../Redux/JotFormSubmissionRedux'
import { PullJotformDataTypes } from '../Redux/PullJotformDataRedux'
import { GetDataFromDbTypes } from '../Redux/GetDataFromDbRedux'

/* ------------- Sagas ------------- */

import { startup } from './StartupSagas'
import { getUserAvatar } from './GithubSagas'
import { jotFormSubmissionRequest } from './JotFormSubmissionSagas'
import { pullJotformDataRequest } from './PullJotformDataSagas'
import { getDataFromDbRequest } from './GetDataFromDbSagas'


/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugConfig.useFixtures ? FixtureAPI : {...Api_Kita.create(), ...Api_Jotform.create()}

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),

    // some sagas receive extra parameters in addition to an action
    takeLatest(GithubTypes.USER_REQUEST, getUserAvatar, api),

    takeLatest(JotFormSubmissionTypes.JOT_FORM_SUBMISSION_REQUEST, jotFormSubmissionRequest, api),

    takeLatest(PullJotformDataTypes.PULL_JOTFORM_DATA_REQUEST, pullJotformDataRequest, api),
    takeLatest(GetDataFromDbTypes.GET_DATA_FROM_DB_REQUEST, getDataFromDbRequest, api),
    
    
  ])
}
